//
// Created by Simon Bone on 12/17/2018.
//
// Linux app to run in background and control the LED Status Indicator
// Receives communication from Streetwise and IPQ Manager via file markers
//
// The class implements the setting of flags that are read by the C-based code

// List of markers...
// * "/sdcard/ipq/led_markers/master_alert"
// * "/sdcard/ipq/led_markers/led_flash1" - LED process clears this flag
// * "/sdcard/ipq/led_markers/led_flash2" - LED process clears this flag
// * "/sdcard/ipq/led_markers/led_flash3" - LED process clears this flag
// *# "/sdcard/ipq/led_markers/streetwise_heartbeat" - LED process clears this flag
// * "/sdcard/ipq/led_markers/good_internet" - LED process clears this flag
// *# "/sdcard/ipq/led_markers/start_test" - LED process clears this flag
// Also looks at
// *# "/sdcard/ipq/states/current_interface
// " /sdcard/ipq/states/is_activated
/*
 *  Build instructions...
 *
 *  This needs to be build manually and copied to the Streetwise project
 *  It's not been integrated into the Streetwise project or the build automated
 *
 *  Select 'app' and then 'build'
 *  the executable we want (armeabi-v7a) will be created in
 *  LedManager\app\build\intermediates\cmake\debug\obj\armeabi-v7a"
 *
 *  The executable should be copied to the streetwise project assets directory
 *  streetwise\app\src\main\assets
 *
 */
#include <stdio.h>
#include <stdlib.h> // access()
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <sys/stat.h>
#include <sys/sysinfo.h> // for uptime()

#define MAXBUFLEN 1000000

//Filename for only_one_instance() lock.
#define INSTANCE_LOCK "led-lock"

// functions to ensure only one instance is running
void only_one_instance(void);
void fail(const char *message);
void ooi_unlink(void);

// functions for interacting with the LED device
void flushACM0 ();
void pauseMs(int ms);
void setColorString(char *colorString);
void initFdForLed();
int checkACM0stat ();

// support functions
int isActivated();
int getCurrentInterface();
long get_uptime();

////////////////////////////////////////////////////////////////////////////////////////////////////
// global variables
//
// fd is file descriptor used to reference /dev/ttyACM0
// fd is set to -1 to represent no valid reference
int fd = -1;
long elapsed_time_of_last_good_internet_marker = -1;        // "/sdcard/led_markers/good_internet"
long elapsed_time_of_last_streetwise_heartbeat_marker = -1; // "/sdcard/led_markers/streetwise_heartbeat"

int main() {

    only_one_instance();

    while (1) {

        // EMERGENCY SIGNAL SUPPORT
        // If this marker is present then flash blue/red automatically
        // and quit
        if ( access("/sdcard/ipq/led_markers/master_alert", F_OK) != -1) {

            printf("master_alert flag detected\n");
            // flash blue/red continuously
            setColorString("B#0000ff-0250#ff0000-0250\r\n");
            // quit the app!
            printf("exit(0)\n");
            exit(0);

        }

        // First check for reset indicator markers...
        // these take precedence
        // first marker - blink white once (return to normal activity)
        // second marker - blink white twice (return to normal activity)
        // third marker - flash blue/off continuously (await reboot..)
        if ( access("/sdcard/ipq/led_markers/led_flash1", F_OK) != -1) {
            printf("/sdcard/ipq/led_markers/led_flash1 detected\n");
            // initial reset marker
            remove("/sdcard/ipq/led_markers/led_flash1"); // delete the marker
            // blink white once
            setColorString("#00ffff\n");
            pauseMs(250);
            setColorString("#0000ff\n");
            pauseMs(250);


        } else if (access("/sdcard/ipq/led_markers/led_flash2", F_OK) != -1) {
            printf("/sdcard/ipq/led_markers/led_flash2 detected\n");
            // second reset marker
            remove("/sdcard/ipq/led_markers/led_flash2"); // delete the marker
            // blink white twice
            setColorString("#00ffff\n");
            pauseMs(250);
            setColorString("#0000ff\n");
            pauseMs(250);
            setColorString("#00ffff\n");
            pauseMs(250);
            setColorString("#0000ff\n");
            pauseMs(250);


        } else if (access("/sdcard/ipq/led_markers/led_flash3", F_OK) != -1) {
            printf("/sdcard/ipq/led_markers/led_flash3 detected\n");
            // second reset marker
            remove("/sdcard/ipq/led_markers/led_flash3"); // delete the marker
            // blink white twice
            setColorString("#00ffff\n");
            pauseMs(250);
            setColorString("#0000ff\n");
            pauseMs(250);
            setColorString("#00ffff\n");
            pauseMs(250);
            setColorString("#0000ff\n");
            pauseMs(250);
            setColorString("#00ffff\n");
            pauseMs(250);
            setColorString("B#0000ff-0250#00ffff-0250\n");
            pauseMs(60000);

        }

        // look for streetwise_heartbeat marker and update elapsed_time tracker
        if (access("/sdcard/ipq/led_markers/streetwise_heartbeat", F_OK) != -1) {

            // final reset marker
            remove("/sdcard/ipq/led_markers/streetwise_heartbeat"); // delete the marker
            elapsed_time_of_last_streetwise_heartbeat_marker = get_uptime();

            printf("streetwise_heartbeat marker detected. elapsed_time_of_last_streetwise_heartbeat_marker = %ld\n", elapsed_time_of_last_streetwise_heartbeat_marker);
        }

        // work out time since last heartbeat
        long time_since_last_heartbeat = get_uptime() - elapsed_time_of_last_streetwise_heartbeat_marker;
        if (elapsed_time_of_last_streetwise_heartbeat_marker == -1) {
            printf("streetwise_heartbeat marker: Not seen since last reset.\n");
        } else {
            printf("streetwise_heartbeat marker: last seen %ld seconds ago\n", time_since_last_heartbeat);
        }

        // Check to see if streetwise has hung
        if ((elapsed_time_of_last_streetwise_heartbeat_marker == -1) ||
                (time_since_last_heartbeat > 60)) {

            printf("More than 60 seconds since last streetwise_heartbeat marker detected. elapsed_time_of_last_streetwise_heartbeat_marker = %ld\n", elapsed_time_of_last_streetwise_heartbeat_marker);

            printf("Streetwise has hung?\n");

            // Flash RED
            setColorString("#ff0000\n");
            pauseMs(1000);
            setColorString("#000000\n");
            pauseMs(1000);

            // jump to start of loop
            continue;

        }






        // look for bad_internet marker and update elapsed_time tracker
        if (access("/sdcard/ipq/led_markers/bad_internet", F_OK) != -1) {

            // final reset marker
            remove("/sdcard/ipq/led_markers/bad_internet"); // delete the marker

            // The desired outcome when this flag is set is for the device
            // status indicators to blink that it's tyring to connect to ethernet/wifi/lte
            // which is triggered when no connection has been available for 60 secs
            // let's force that trigger by setting this to 60 secs in the past
//            elapsed_time_of_last_good_internet_marker = -1;
            elapsed_time_of_last_good_internet_marker = get_uptime() - 61;

            printf("bad_internet marker detected. elapsed_time_of_last_good_internet_marker = %ld\n", elapsed_time_of_last_good_internet_marker);
        }


        // look for good_internet marker and update elapsed_time tracker
        if (access("/sdcard/ipq/led_markers/good_internet", F_OK) != -1) {

            // final reset marker
            remove("/sdcard/ipq/led_markers/good_internet"); // delete the marker
            elapsed_time_of_last_good_internet_marker = get_uptime();

            printf("good_internet marker detected. elapsed_time_of_last_good_internet_marker = %ld\n", elapsed_time_of_last_good_internet_marker);
        }


        // work out time since last good_internet_marker
        long time_since_good_internet = get_uptime() - elapsed_time_of_last_good_internet_marker;
        if (elapsed_time_of_last_good_internet_marker == -1) {
            printf("good_internet marker: Not seen since last reset.\n");
        } else {
            printf("good_internet marker: last seen %ld seconds ago\n", time_since_good_internet);
        }


        // Now check for device state-based indicators
        if (!isActivated()) {

            printf("Probe is not activated.\n");

            if ((elapsed_time_of_last_good_internet_marker == -1) ||
                (time_since_good_internet > 60)) {

                printf("More than 60 seconds since last good_internet marker detected. elapsed_time_of_last_good_internet_marker = %ld\n", elapsed_time_of_last_good_internet_marker);

                // Flash WHITE
                setColorString("#ffffff\n");
                pauseMs(1000);
                setColorString("#000000\n");
                pauseMs(1000);

            } else {

                // Set to WHITE
                setColorString("#ffffff\n");
                pauseMs(1000);

            }

            // jump to top of loop
            continue;

        }


        // Check for no network connection for 3 minutes or more
        if ((elapsed_time_of_last_good_internet_marker == -1) ||
            (time_since_good_internet > 180)) {

            printf("More than 180 seconds since last good_internet marker detected. elapsed_time_of_last_good_internet_marker = %ld\n", elapsed_time_of_last_good_internet_marker);

            // Set to RED
            setColorString("#ff0000\n");
            pauseMs(1000);

            // jump to top of loop
            continue;

        }

        // Check for start_test marker
        if ( access("/sdcard/ipq/led_markers/start_test", F_OK) != -1) {

            printf("/sdcard/ipq/led_markers/start_test detected\n");

            remove("/sdcard/ipq/led_markers/start_test"); // delete the marker
            setColorString("#ffffff\n");
            pauseMs(1000);

        }


        // Display color indicating network current interface
        // Solid = Connected, Flashing = Not Connected
        // ETHERNET = YELLOW
        // WIFI = GREEN
        // LTE = PURPLE

        printf("Probe is activated.\n");

        int interface = getCurrentInterface();

        switch (interface) {
            case 1: // ETHERNET

                if ((elapsed_time_of_last_good_internet_marker == -1) ||
                    (time_since_good_internet > 60)) {

                    printf("More than 60 seconds since last good_internet marker detected. elapsed_time_of_last_good_internet_marker = %ld\n", elapsed_time_of_last_good_internet_marker);

                    // Flash YELLOW
                    setColorString("#ffff00\n");
                    pauseMs(1000);
                    setColorString("#000000\n");
                    pauseMs(1000);

                } else {

                    // Set to YELLOW
                    setColorString("#ffff00\n");
                    pauseMs(1000);

                }

                break;
            case 2: // WIFI

                if ((elapsed_time_of_last_good_internet_marker == -1) ||
                    (time_since_good_internet > 60)) {

                    printf("More than 60 seconds since last good_internet marker detected. elapsed_time_of_last_good_internet_marker = %ld\n", elapsed_time_of_last_good_internet_marker);

                    // Flash GREEN
                    setColorString("#00ff00\n");
                    pauseMs(1000);
                    setColorString("#000000\n");
                    pauseMs(1000);

                } else {

                    // Set to GREEN
                    setColorString("#00ff00\n");
                    pauseMs(1000);

                }
                break;
            case 3: // LTE

                if ((elapsed_time_of_last_good_internet_marker == -1) ||
                    (time_since_good_internet > 60)) {

                    printf("More than 60 seconds since last good_internet marker detected. elapsed_time_of_last_good_internet_marker = %ld\n", elapsed_time_of_last_good_internet_marker);

                    // Flash PURPLE
                    setColorString("#ff00ff\n");
                    pauseMs(1000);
                    setColorString("#000000\n");
                    pauseMs(1000);

                } else {

                    // Set to PURPLE
                    setColorString("#ff00ff\n");
                    pauseMs(1000);

                }
                break;
            default:
                pauseMs(1000);
                break;
        }





    }
}



// function to initialize the connection to the led device if needed
// and clean-up old fd reference if the led device is no longer available
// (the led device can be inserted/removed)
void initLedDeviceIfNeeded() {

    int deviceAvailableNow = checkACM0stat();

    if (fd == -1) {
        printf("initLedDeviceIfNeeded() device not previously setup\n");

        if (deviceAvailableNow == 1) {
            printf("initLedDeviceIfNeeded() device now available :) setting up....\n");
            initFdForLed();
        } else {
            printf("initLedDeviceIfNeeded() device not available :(\n");
        }

    } else {
        printf("initLedDeviceIfNeeded() device was previously setup.\n");

        if (deviceAvailableNow == 1) {
            printf("initLedDeviceIfNeeded() device still available :) yay!\n");
        } else {
            printf("initLedDeviceIfNeeded() device no longer available :(\n");
            close(fd);
            fd = -1;
        }

    }

}

// check if the led device is available
// returns 1 if yes, 0 if no
int checkACM0stat () {

    printf("Checking stat() for /dev/ttyACM0...\n");
    struct stat sb;
    if (stat("/dev/ttyACM0", &sb) == 0) {
        printf("/dev/ttyACM0 stat() SUCCESS\n");
        return 1;
    } else {
        printf("/dev/ttyACM0 stat() FAIL\n");
        return 0;
    }

}


// open connection to led device
void initFdForLed() {
    printf("Connecting to LED...\n");
    printf("Opening /dev/ttyACM0\n");
    // O_WRONLY = Open for writing only
    // O_NONBLOCK = An open() for writing only will return an error if no process currently has the file open for reading.
    // O_NOCTTY = If set and path identifies a terminal device, open() will not cause the terminal device to become the controlling terminal for the process
    // O_SYNC = Write I/O operations on the file descriptor complete as defined by synchronised I/O file integrity completion.
    fd= open("/dev/ttyACM0",O_WRONLY | O_NONBLOCK | O_NOCTTY );
    //fd= open("/dev/ttyACM0",O_WRONLY|O_NONBLOCK|O_NOCTTY|O_SYNC );
    if(fd == -1)
    {
        printf("Unable to open /dev/ttyACM0\n");
        return;
    }
    printf("/dev/ttyACM0 opened OK\n");
    printf("Connected to LED\n");
    struct termios tty;
    cfmakeraw(&tty);
    tty.c_cflag=CRTSCTS|HUPCL|CS8|CLOCAL;
    cfsetospeed(&tty,B9600);
    cfsetispeed(&tty,B9600);
    int tcsetattr_result = tcsetattr(fd,TCSANOW,&tty);
    printf("tcsetattr_result = %i\n", tcsetattr_result);

    int bytes_written = 0;


    printf("Setting fade to 1ms...\n");
    bytes_written = write(fd,"F0001\n",6);
    printf("Bytes written = %i\n", bytes_written);
    if(bytes_written == -1) {
        printf("Oh dear, something went wrong with write(). Error = %s\n", strerror(errno));
    }
    flushACM0();

}

// function to pause specified number of ms
void pauseMs(int ms) {
    printf("Pausing %i ms...\n", ms);
    usleep(ms * 1000);
}


void setColorString(char *colorString) {

    initLedDeviceIfNeeded();

    if (fd == -1) {
        printf("setColorString() device not available");
        return;
    }

    int len = strlen(colorString);
    printf("Setting string %sString length = %d\n", colorString, len);

    int bytes_written = write(fd,colorString,len);
    printf("Bytes written = %i\n", bytes_written);
    if(bytes_written == -1) {
        printf("Oh dear, something went wrong with write(). Error = %s\n", strerror(errno));
    }
    flushACM0();


}


// flush non-transmitted both output data AND non-read input data
// without regular calls to this the led device can become unresponsive
void flushACM0 () {
    int flush_result = 0;
    printf("Flushing...\n");
    flush_result = tcflush(fd,TCIOFLUSH );
    printf("flush_result = %i\n", flush_result);
}


// function to determine if the device is activated
// returns 1 if yes, 0 if no
int isActivated() {

    // when probe is activated the content of
    // /sdcard/ipq/states/is_activated is "true"

    char source[MAXBUFLEN + 1];
    FILE *fp = fopen("/sdcard/ipq/states/is_activated", "r");
    if (fp != NULL) {
        size_t newLen = fread(source, sizeof(char), MAXBUFLEN, fp);
        if (newLen == 0) {
            printf("isActivated() unable to read file\n");
        } else {
            source[newLen] = '\0'; /* Just to be safe. */
            printf("isActivated() content is '%s'\n", source);
        }
        fclose(fp);

        if (strncmp(source, "true", strlen("true")) == 0) {
            printf("isActivated() device is activated\n" );
            return 1;
        }
    } else {
        printf("isActivated() unable to open file\n");
    }
    printf("isActivated() device is not activated\n" );
    return 0;
}

// function to determine the current network interface accessed by streetwise
// returns 1 = ETHERNET
// returns 2 = WIFI
// returns 3 = LTE
// returns -1 = UNKNOWN
int getCurrentInterface() {

    // current interface is stored in
    // /sdcard/ipq/states/current_interface
    // "WIFI", "ETHERNET, "LTE"

    int interface = -1;

    char source[MAXBUFLEN + 1];
    FILE *fp = fopen("/sdcard/ipq/states/current_interface", "r");
    if (fp != NULL) {
        size_t newLen = fread(source, sizeof(char), MAXBUFLEN, fp);
        if (newLen == 0) {
            printf("getCurrentInterface() unable to read file\n");
        } else {
            source[newLen] = '\0'; /* Just to be safe. */
            printf("getCurrentInterface() content is '%s'\n", source);
        }
        fclose(fp);

        if (strncmp(source, "ETHERNET", strlen("ETHERNET")) == 0) {
            printf("getCurrentInterface() interface is 1 (ETHERNET)\n" );
            interface = 1;
        } else if (strncmp(source, "WIFI", strlen("WIFI")) == 0) {
            printf("getCurrentInterface() interface is 2 (WIFI)\n" );
            interface = 2;
        } else if (strncmp(source, "LTE", strlen("LTE")) == 0) {
            printf("getCurrentInterface() interface is 3 (LTE)\n" );
            interface = 3;
        }

    } else {
        printf("isActivated() unable to open file\n");
    }

    return interface;
}


long get_uptime()
{
    struct sysinfo s_info;
    int error = sysinfo(&s_info);
    if(error != 0)
    {
        printf("code error = %d\n", error);
    }
    return s_info.uptime;
}

//////////////////////////////////////////////////////////////////////////////////////////////////

// Code to ensure only one instance is running
// From https://rosettacode.org/wiki/Determine_if_only_one_instance_is_running#C

void
fail(const char *message)
{
    perror(message);
    exit(1);
}

/* Path to only_one_instance() lock. */
static char *ooi_path;

void
ooi_unlink(void)
{
    unlink(ooi_path);
}

/* Exit if another instance of this program is running. */
void only_one_instance(void)
{
    struct flock fl;
    size_t dirlen;
    int fd;
    char *dir;

    dir = "/sdcard";
    dirlen = strlen(dir);

    ooi_path = malloc(dirlen + sizeof("/" INSTANCE_LOCK));
    if (ooi_path == NULL)
        fail("malloc");
    memcpy(ooi_path, dir, dirlen);
    memcpy(ooi_path + dirlen, "/" INSTANCE_LOCK,
           sizeof("/" INSTANCE_LOCK));  /* copies '\0' */

    fd = open(ooi_path, O_RDWR | O_CREAT, 0600);
    if (fd < 0)
        fail(ooi_path);

    fl.l_start = 0;
    fl.l_len = 0;
    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    if (fcntl(fd, F_SETLK, &fl) < 0) {
        fputs("Another instance of this program is running.\n",
              stderr);
        exit(1);
    }

    printf("Launched\n");

    /*
     * Run unlink(ooi_path) when the program exits. The program
     * always releases locks when it exits.
     */
    atexit(ooi_unlink);
}


